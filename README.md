# Software Engineering Assignmennt - Web POS

## Deployed at

[Web POS](https://web-pos-seven.vercel.app/)

## Run on local host

### Install missing packages using the command:

```
yarn install
```

### Run the app in the development mode using the command:

```
yarn start
```

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Launches the test runner in the interactive watch mode using the command:

```
yarn test
```

### Build the web and be ready for deploying using the command:

```
yarn build
```

## Technologies

- ReactJS

- Typescript

- Google Firebase
