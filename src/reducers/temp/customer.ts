import Dish from '../../models/Dish'

export type DishCustomer = {
  dish: Dish
  amount: number
  status: 'Uncomplete' | 'Complete'
}