import Dish from '../models/Dish'

import {
  ADD_DISH_TO_LIST,
  GET_ALL_DISHES,
  SEARCH_DISHES,
  UPDATE_ONCE,
  REMOVE_DISH_FROM_LIST,
  SEARCH_CATEGORY,
} from '../constants'

const initialState: Dish[] = []

type ActionType = {
  type: string
  payload: any
}

const dishes = (state = initialState, action: ActionType) => {
  switch (action.type) {
    case GET_ALL_DISHES: {
      const dishes: Dish[] = action.payload
      return dishes
    }
    case SEARCH_DISHES: {
      const dishes: Dish[] = action.payload

      return dishes
    }
    case SEARCH_CATEGORY: {
      const dishes: Dish[] = action.payload

      return dishes
    }
    case UPDATE_ONCE: {
      const { dish } = action.payload
      const newState = state.map((d) => {
        return dish.id === d.id ? dish : d
      })

      return newState
    }
    case ADD_DISH_TO_LIST: {
      const newDish = action.payload
      return [...state, newDish]
    }
    case REMOVE_DISH_FROM_LIST: {
      const { id } = action.payload
      const newState = state.filter((i) => i.id !== id)
      return newState
    }
    default:
      return state
  }
}

export default dishes
