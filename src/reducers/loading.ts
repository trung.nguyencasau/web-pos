import { START_LOADING, END_LOADING } from '../constants'

const initialState: boolean = false

const loading = (state = initialState, action: any) => {
  switch (action.type) {
    case START_LOADING:
      return true
    case END_LOADING:
      return false
    default:
      return state
  }
}

export default loading
