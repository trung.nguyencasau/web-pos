import { SET_INFO } from '../constants'

type ActionProps = {
  type: string
  payload: any
}

const initialState = {
  tableId: '',
}

const info = (state = initialState, action: ActionProps) => {
  switch (action.type) {
    case SET_INFO:
      return {
        ...state,
        tableId: action.payload,
      }
    default:
      return state
  }
}

export default info
