import { combineReducers } from 'redux'

import order from './order'
import chosenDish from './chosenDish'
import dishes from './dishes'
import loading from './loading'
import auth from './auth'
import info from './info'

export const reducers = combineReducers({
  order,
  chosenDish,
  dishes,
  loading,
  auth,
  info,
})
