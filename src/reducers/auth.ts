import { GET_USER, LOG_OUT } from '../constants'
import User from '../models/User';

const initialState: User | null = null;

const authReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case GET_USER: {
            return action.payload;
        }
        case LOG_OUT:
            return null;
        default:
            return state;
    }
}

export default authReducer