import {
  CHANGE_DISH_FIELD,
  ADD_DISH,
  MINUS_AMOUNT_DISH,
  PLUS_AMOUNT_DISH,
  VIEW_DISH,
  CLEAR_CHOSEN_DISH,
} from '../constants'

import { DishCustomer } from './temp/customer'

import Dish from '../models/Dish'

type ActionType = {
  type: string
  payload?: any
}

const initialState: DishCustomer = {
  dish: new Dish('', '123', '', 0, '', 'Available', ''),
  amount: 0,
  status: 'Uncomplete',
}

const chosenDishes = (state = initialState, action: ActionType) => {
  switch (action.type) {
    case PLUS_AMOUNT_DISH:
      return {
        ...state,
        amount: state.amount + 1,
      }
    case MINUS_AMOUNT_DISH: {
      return {
        ...state,
        amount: state.amount === 1 ? 1 : state.amount - 1,
      }
    }
    case ADD_DISH: {
      const { dish, amount } = action.payload
      const newDish = new Dish(
        dish.id,
        dish.resID,
        dish.name,
        dish.price,
        dish.description,
        dish.status,
        dish.image
      )

      return {
        ...state,
        dish: newDish,
        amount,
      }
    }
    case VIEW_DISH: {
      const { dish } = action.payload
      return {
        ...state,
        dish: dish,
      }
    }
    case CHANGE_DISH_FIELD: {
      const { key, value } = action.payload
      const newDish = { ...state.dish, [key]: value }

      return {
        ...state,
        dish: newDish,
      }
    }
    case CLEAR_CHOSEN_DISH: {
      return initialState
    }
    default:
      return state
  }
}

export default chosenDishes
