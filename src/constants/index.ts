export const START_LOADING = 'START_LOADING'
export const END_LOADING = 'END_LOADING'

// Dish temp
export const ADD_DISH = 'ADD_DISH'
export const MINUS_AMOUNT_DISH = 'MINUS_AMOUNT_DISH'
export const PLUS_AMOUNT_DISH = 'PLUS_AMOUNT_DISH'

// Order temp
export const ADD_DISH_TO_ORDER = 'ADD_DISH_TO_ORDER'
export const REMOVE_DISH_FROM_ORDER = 'REMOVE_DISH_FROM_ORDER'
export const MINUS_ORDER = 'MINUS_ORDER'
export const PLUS_ORDER = 'PLUS_ORDER'
export const CANCEL_ORDER = 'CANCEL_ORDER'
export const ORDER_UPDATE_STATUS_ACCEPT = 'ORDER_UPDATE_STATUS_ACCEPT'
export const ORDER_UPDATE_ID = 'ORDER_UPDATE_ID'

// Customer dish
export const GET_ALL_DISHES = 'GET_ALL_DISHES'
export const SEARCH_DISHES = 'SEARCH_DISHES'
export const SEARCH_CATEGORY = 'SEARCH_CATEGORY'

export const GET_USER = 'GET_USER'
export const EDIT_PROFILE = 'EDIT_PROFILE'
export const LOG_OUT = 'LOGOUT'

// Info
export const SET_INFO = 'SET_INFO'

// Dish for customer
export const VIEW_DISH = 'VIEW_DISH'
export const CHANGE_DISH_FIELD = 'CHANGE_DISH_FIELD'
export const UPDATE_ONCE = 'UPDATE_ONCE'
export const CLEAR_CHOSEN_DISH = 'CLEAR_CHOSEN_DISH'
export const ADD_DISH_TO_LIST = 'ADD_DISH_TO_LIST'
export const REMOVE_DISH_FROM_LIST = 'REMOVE_DISH_FROM_LIST'
