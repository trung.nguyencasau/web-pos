import Dish from "../models/Dish";
import UserController from "./UserController";
import "firebase/compat/firestore";
import firebase from "../configs/firebase";

export default class ManagerController extends UserController {
    static async createDish(dish: Dish): Promise<boolean> {
        try {
            await firebase.firestore().collection("restaurants").doc(dish.resId).collection("dishes").doc(dish.id).set({
                'name': dish.name, 
                'price': dish.price, 
                'description': dish.description, 
                'status': dish.status, 
                'image': dish.image,
                'category': dish.category
            });
            return true;
        } catch (error) {
            return false;
        }
    }

    static async updateDish(dish: Dish): Promise<boolean> {
        try {
            await firebase.firestore().collection("restaurants").doc(dish.resId).collection("dishes").doc(dish.id).update({
                'name': dish.name, 
                'price': dish.price, 
                'description': dish.description, 
                'status': dish.status, 
                'image': dish.image,
            });
            return true;
        } catch (error) {
            return false;
        }
    }

    static async deleteDish(resId: string, dishId: string): Promise<boolean> {
        try {
            await firebase.firestore().collection("restaurants").doc(resId).collection("dishes").doc(dishId).update({
                'status': 'Delete',
            });
            return true;
        } catch (error) {
            return false;
        }
    }

    static async addEmployee(resId: string, email: string, role: 'Clerk' | 'Kitchen'): Promise<boolean> {
        var employee
        try {
            var employeeData = await firebase.firestore().collection("users").where("email", "==", email).where("role", "==", "None").limit(1).get()
            if (employeeData.docs === undefined || employeeData.docs.length < 1) {
                return false;
            }
            employee = employeeData.docs[0];
            await firebase.firestore().collection("users").doc(employee.id).update({
                'role': role,
                'restaurant': firebase.firestore().collection("restaurants").doc(resId),
            })
        } catch (error) {
            return false
        }

        try {
            await firebase.firestore().collection("restaurants").doc(resId).update({
                "staffEmail": firebase.firestore.FieldValue.arrayUnion(email),
            })
            return true
        } catch (error) {
            await firebase.firestore().collection("users").doc(employee.id).update({
                'role': 'None',
                'restaurant': null,
            })
            return false
        }
    }

    static async deleteEmployee(resId: string, email: string): Promise<boolean> {
        var employee
        try {
            var employeeData = await firebase.firestore().collection("users").where("email", "==", email)
                .where("restaurant", "==", firebase.firestore().collection("restaurants").doc(resId)).limit(1).get()
            if (employeeData.docs === undefined || employeeData.docs.length < 1) return false;
            employee = employeeData.docs[0];
            await firebase.firestore().collection("users").doc(employee.id).update({
                'role': 'None',
                'restaurant': null,
            })
        } catch (error) {
            return false
        }

        try {
            await firebase.firestore().collection("restaurants").doc(resId).update({
                "staffEmail": firebase.firestore.FieldValue.arrayRemove(email),
            })
            return true
        } catch (error) {
            await firebase.firestore().collection("users").doc(employee.id).update({
                'role': employee.get("role"),
                'restaurant': employee.get("restaurant"),
            })
            return false
        }
    }
}