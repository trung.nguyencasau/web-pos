import Dish from "../models/Dish";
import Order from "../models/Order";
import Table from "../models/Table";
import Restaurant from "../models/Restaurant";
import UserController from "./UserController";
import "firebase/compat/firestore";
import firebase from "../configs/firebase";

export default class CustomerController extends UserController {
    static async getRestaurantInfo(resId: string, tableId: string): Promise<[Restaurant, Table] | null> {
        try {
            var doc = firebase.firestore().collection("restaurants").doc(resId);
            var resData = await doc.get();
            var tableData = await doc.collection("tables").doc(tableId).get();
            if (!resData.exists || !tableData.exists) return null;
            else {
                return [
                    new Restaurant(
                        resId,
                        resData.get("name"),
                        resData.get("staffEmail"),
                        resData.get("managerEmail")
                    ),
                    new Table(
                        tableData.id,
                        tableData.get("status")
                    )
                ];
            }      
        }
        catch (error) {
            return null;
        }
    }

    static async searchDish(resId: string, keyword: string): Promise<Dish[] | null> {
        try {
            var dishList: Dish[] = [];
            var query = await firebase.firestore().collection("restaurants").doc(resId).collection("dishes").where("name", ">=", keyword).where("name", "<=", keyword + '\uf8ff').get();
            query.forEach((dish) => {
                dishList.push(new Dish(
                    dish.id,
                    resId,
                    dish.get("name"),
                    Number(dish.get("price")),
                    dish.get("description"),
                    dish.get("status"),
                    dish.get("image")
                ));
            });
            return dishList;
        }
        catch (error) {
            return null;
        }
    }

    static async finishPayment(resId: string, payId: string): Promise<boolean> {
        try {
            await firebase.firestore().collection("restaurants").doc(resId).collection("payments").doc(payId).update({
                status: 'Finish'
            });
            return true;
        }
        catch (error) {
            return false;
        }
    }

    static async sendOrder(order: Order, onReceive?: () => void): Promise<boolean> {
        try {
            await firebase.firestore().collection("restaurants").doc(order.resId).collection("orders").doc(order.id).set({
                createAt: order.createAt,
                listOfDishes: order.listOfDishes.map((val) => {
                    return {
                        dish: firebase.firestore().collection("restaurants").doc(order.resId).collection("dishes").doc(val.dish.id),
                        amount: val.amount,
                        status: val.status
                    };
                }),
                status: order.status,
                owner: order.owner,
                table: firebase.firestore().collection("restaurants").doc(order.resId).collection("tables").doc(order.table.id)
            });
            var unsubscribe = firebase.firestore().collection("restaurants").doc(order.resId).collection("orders").doc(order.id).onSnapshot(
                (orderSnapshot) => {
                    if (orderSnapshot.get("status") !== 'Not') {
                        // Payment has id the same as orderid
                        firebase.firestore().collection("restaurants").doc(order.resId).collection("payments").doc(order.id).set({
                            createdAt: firebase.firestore.Timestamp.now().toDate(),
                            order: firebase.firestore().collection("restaurants").doc(order.resId).collection("orders").doc(order.id),
                            status: 'Not'
                        }).then(
                            // Change UI after payment is allowed
                            onReceive
                        );
                        unsubscribe();
                    }
                }
            )
            return true;
        }
        catch (error) {
            return false;
        }
    }
}