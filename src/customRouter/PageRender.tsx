import React from 'react'
import { useParams } from 'react-router-dom'
import { useSelector } from 'react-redux'

const generatePage = (pageName: string) => {
    const component = () => require(`../pages/${pageName}`).default;
    if (pageName === "ProfilePage")
        window.history.pushState('', '', '/profile');
    try {
        return React.createElement(component())
    } catch (err) {
        return <div></div>
    }
}

const PageRender = () => {
    const { page }: any = useParams()
    const { auth }: any = useSelector(state => state)

    let managerMap = new Map([
        ["profile", "ProfilePage"],
        ["manager", "ManageDishPage"],
        ["clerk_order", "ClerkOrderPage"],
        ["clerk_payment", "ClerkPaymentPage"],
        ["clerk_order_status", "ClerkOrderStatusPage"],
        ["table", "ClerkTablePage"],
        ["kitchen_order", "KitchenOrderPage"],
    ]);

    let clerkMap = new Map([
        ["profile", "ProfilePage"],
        ["clerk_order", "ClerkOrderPage"],
        ["table", "ClerkTablePage"],
        ["clerk_order_status", "ClerkOrderStatusPage"],
        ["clerk_payment", "ClerkPaymentPage"],
    ]);

    let kitchenMap = new Map([
        ["profile", "ProfilePage"],
        ["kitchen_order", "KitchenOrderPage"]
    ]);

    let userMap = new Map([
        ["profile", "ProfilePage"],
    ])

    let pageName: string | undefined;

    if (auth) {
        if (auth.role === "Manager") {
            pageName = managerMap.get(page);
        }
        else if (auth.role === "Clerk") {
            pageName = clerkMap.get(page);
        }
        else if (auth.role === "Kitchen") {
            pageName = kitchenMap.get(page);
        }
        else if (auth.role === "None" && auth.id !== "") {
            pageName = userMap.get(page);
        }
    }
    pageName = pageName ? pageName : "ProfilePage";
    return generatePage(pageName)
}

export default PageRender