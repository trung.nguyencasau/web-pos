import { Route, Redirect} from 'react-router-dom';
import { useSelector } from 'react-redux';

const PrivateRouter = (props: any) => {
    const {auth}: any = useSelector(state => state);
    return auth ? <Route {...props} /> : <Redirect to="/" />
}

export default PrivateRouter