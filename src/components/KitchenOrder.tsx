import { Grid, Typography, Checkbox, TextField } from '@mui/material'
import React from 'react'

import { DishCustomer } from '../reducers/temp/customer'

interface KitchenOrderProps {
    id: string
    listOfDishes: DishCustomer[]
    handleFinishDishInOrder: (orderId: string, dishId: string) => Promise<void>
}

const KitchenOrder = (props: KitchenOrderProps) => {
    return (
        <Grid container boxShadow={2} direction='column' sx={{ borderRadius: '10px', paddingX: '20px', paddingY: '10px', mt: 3 }}>
            <Grid item xs={12} sx={{ mb: 1 }}>
                <Typography variant="body1" fontWeight="bold">
                    Order #{props.id}
                </Typography>
            </Grid>
            {
                props.listOfDishes.map((item: DishCustomer) => {
                    return (
                        <Grid item
                            container
                            justifyContent="space-between"
                            alignItems="center"
                            paddingY="5px"
                            paddingX="20px"
                            sx={{ border: '1px solid #E5E5E5', borderRadius: '10px', marginBottom: '10px' }}
                            key={item.dish.id}>
                            <Grid item xs={1}>
                                <Checkbox
                                    sx={{
                                        color: 'black',
                                        paddingLeft: '0px',
                                        '&.Mui-checked': { color: '#A2F07B' },
                                    }}
                                    disabled={item.status === 'Complete' ? true : false}
                                    onClick={() => props.handleFinishDishInOrder(props.id, item.dish.id)}
                                />
                            </Grid>
                            <Grid item xs={8}>
                                <Typography variant="body2">
                                    {item.dish.name}
                                </Typography>
                            </Grid>
                            <Grid item xs={1}>
                                <Typography variant="body2">
                                    {item.dish.price}
                                </Typography>
                            </Grid>
                            <Grid item xs={2} textAlign='right'>
                                <TextField
                                    disabled
                                    sx={{ width: '30px' }}
                                    inputProps={{
                                        style: {
                                            fontSize: '14px',
                                            padding: '4px',
                                            textAlign: 'center',
                                        },
                                    }}
                                    defaultValue={item.amount}
                                />
                            </Grid>
                        </Grid>
                    )
                })
            }
        </Grid>
    )
}

export default KitchenOrder