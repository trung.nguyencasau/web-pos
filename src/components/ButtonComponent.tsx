import React from 'react'

import { Button } from '@mui/material'

interface ButtonProps {
  children: React.ReactNode | undefined,
  size: 'medium' | 'small' | 'large',
  color:
  | 'primaryColor'
  | 'lightColor'
  | 'textColor'
  | 'editColor'
  | 'greenColor'
  | 'titleColor',
  variant: 'text' | 'contained' | 'outlined' | undefined,
  sx?: object,
  fullWidth?: boolean,
  startIcon?: React.ReactNode,
  endIcon?: React.ReactNode,
  disabled?: boolean
  onClick?: (event: any | MouseEvent) => any
}

const ButtonComponent = (props: ButtonProps) => {
  return (
    <div>
      <Button
        sx={props.sx}
        variant={props.variant}
        color={props.color}
        startIcon={props.startIcon}
        endIcon={props.endIcon}
        disabled={props.disabled}
        size={props.size}
        fullWidth={props.fullWidth}
        onClick={props.onClick}
      >
        {props.children}
      </Button>
    </div>
  )
}

export default ButtonComponent
