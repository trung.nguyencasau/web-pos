import React from "react";
import { Grid, Typography } from "@mui/material";
//import { Typography } from "@material-ui/core";
import ButtonIconComponent from "./ButtonIconComponent";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import AuthenticateController from '../controllers/AuthenticateController';
import {LOG_OUT} from '../constants';
import { useHistory } from 'react-router';
import { useDispatch } from 'react-redux';

interface StaffHeaderProp {
  page: string;
}

const StaffHeader = (props: StaffHeaderProp) => {
  const history = useHistory();
  const dispatch = useDispatch();

  const logout = async() => {
    await AuthenticateController.logout();
    dispatch({type: LOG_OUT});
    history.push("/");
  }

  return (
    <Grid container justifyContent="space-between" alignItems="center">
      <Grid item xs={7}>
        <Typography variant="h6" sx={{ fontSize: "24px", fontWeight: "700" }}>
          {props.page}
        </Typography>
      </Grid>
      <Grid item xs={2}>
        <Grid container justifyContent="flex-end">
          <ButtonIconComponent
            py={5}
            px={20}
            borderRadius={10}
            children={<ExitToAppIcon sx={{ fontSize: "28px" }} />}
            color="lightColor"
            size="small"
            backgroundColor="primaryColor"
            onClick={logout}
          />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default StaffHeader;
