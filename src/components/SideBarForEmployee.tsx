import React from 'react'
import { useHistory } from 'react-router-dom'

import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';
import AppRegistrationIcon from '@mui/icons-material/AppRegistration';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import PaymentIcon from '@mui/icons-material/Payment';
import DiningIcon from '@mui/icons-material/Dining';
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
import AssistantPhotoIcon from '@mui/icons-material/AssistantPhoto';

import SideBarComponent from './SideBarComponent'

const SideBarForEmployee = () => {
    const history = useHistory()

    const handleChangeToManager = () => {
        history.push('/manager')
    }
    const handleChangeToProfile = () => {
        history.push('/profile')
    }
    const handleChangeToClerk = () => {
        history.push('/clerk_order')
    }
    const handleChangeToKitchenOrder = () => {
        history.push('/kitchen_order')
    }
    const handleChangeToPayment = () => {
        history.push('/clerk_payment')
    }
    const handleChangeToClerkStatus = () => {
        history.push('/clerk_order_status')
    }
    const handleChangeToTable = () => {
        history.push('/table')
    }
    return (
        <SideBarComponent list={[{ icon: <AssignmentIndIcon />, label: 'Profile', handleChangeLink: handleChangeToProfile },
        { icon: <AppRegistrationIcon />, label: 'Table', handleChangeLink: handleChangeToTable },
        { icon: <ShoppingCartIcon />, label: 'Order', handleChangeLink: handleChangeToClerk },
        { icon: <AssistantPhotoIcon />, label: 'Order status', handleChangeLink: handleChangeToClerkStatus },
        { icon: <PaymentIcon />, label: 'Payment', handleChangeLink: handleChangeToPayment },
        { icon: <DiningIcon />, label: 'Kitchen', handleChangeLink: handleChangeToKitchenOrder },
        { icon: <ManageAccountsIcon />, label: "Dishes management", handleChangeLink: handleChangeToManager },
        ]
        }
        />
    )
}

export default SideBarForEmployee