import React from 'react'

import { Grid, Typography } from '@mui/material'

interface PricePaymentProps {
  subTotal: number
}

const PricePayment = (props: PricePaymentProps) => {
  return (
    <Grid
      container
      sx={{
        maxWidth: '270px',
      }}
    >
      <Grid item container justifyContent="space-between" alignItems="center">
        <Typography variant="body1" sx={{ fontWeight: 'bold' }}>
          Subtotal:
        </Typography>
        <Typography variant="body2" sx={{ fontWeight: 'bold' }}>
          {props.subTotal.toLocaleString('en-US', {
            style: 'currency',
            currency: 'USD',
          })}
        </Typography>
      </Grid>
      <Grid
        item
        container
        justifyContent="space-between"
        alignItems="center"
        sx={{ my: 2, pb: 1, borderBottom: '1px dashed #e4e4e4' }}
      >
        <Typography variant="body1">Tax (10%):</Typography>
        <Typography variant="body2">
          {(0.1 * props.subTotal).toLocaleString('en-US', {
            style: 'currency',
            currency: 'USD',
          })}
        </Typography>
      </Grid>
      <Grid item container justifyContent="space-between" alignItems="center">
        <Typography variant="body1" sx={{ fontWeight: 'bold' }}>
          Total:
        </Typography>
        <Typography variant="body2" sx={{ fontWeight: 'bold' }}>
          {(1.1 * props.subTotal).toLocaleString('en-US', {
            style: 'currency',
            currency: 'USD',
          })}
        </Typography>
      </Grid>
    </Grid>
  )
}

export default PricePayment
