import React from 'react'
import { Grid, Typography } from '@mui/material'
import CancelIcon from '@mui/icons-material/Cancel';
import ButtonIconComponent from './ButtonIconComponent';


interface ClerkOrderCompleteProps{
    id: string
}

const ClerkOrderComplete = (props: ClerkOrderCompleteProps) => {
    return (
        <Grid container justifyContent='space-between' alignItems = 'center' sx = {{backgroundColor: 'white', borderRadius: '10px', paddingX: '20px', paddingY: '10px'}}>
            <Grid item>
                <Typography variant='body1' fontWeight='bold'>
                    Order #{props.id}
                </Typography>
            </Grid>
            <Grid item>
                <ButtonIconComponent 
                backgroundColor = 'lightColor'
                children = {<CancelIcon/>}
                size = 'small'
                color = 'titleColor'/>
            </Grid>
        </Grid>
    )
}

export default ClerkOrderComplete