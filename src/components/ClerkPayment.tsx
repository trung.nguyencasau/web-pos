import React, { useState } from 'react'
import { Grid, Typography } from '@mui/material'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import ButtonComponent from '../components/ButtonComponent'

import Dish from '../models/Dish'
interface PaymentItemProps {
    id: string //id of order
    listOfDishes: { dish: Dish, amount: number, status: 'Uncomplete' | 'Complete' }[]
    handleClerkConfirmPayment: (id: string) => Promise<void>
}

const ClerkPayment = (props: PaymentItemProps) => {
    const [readMore, setReadMore] = useState<boolean>(false)
    const handleChangeReadMore = () => {
        setReadMore(!readMore)
    }
    return (
        <Grid container boxShadow={2} borderRadius='10px' sx={{ mb: 3 }}>
            <Grid item padding="2px" container sx={{ height: '40px', backgroundColor: '#A2F07B', borderTopLeftRadius: '10px', borderTopRightRadius: '10px' }} alignItems='center'>
                <Grid item xs={1} textAlign="center">
                    <Typography variant="body2" textAlign="center" fontWeight='bold' sx={{ paddingLeft: '5px' }}>
                        #{props.id}
                    </Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography variant="body2" textAlign="center" fontWeight='bold'>
                        Name
                    </Typography>
                </Grid>
                <Grid item xs={2}>
                    <Typography variant="body2" textAlign="center" fontWeight='bold'>
                        Price
                    </Typography>
                </Grid>
                <Grid item xs={2}>
                    <Typography variant="body2" textAlign="center" fontWeight='bold'>
                        Amount
                    </Typography>
                </Grid>
                <Grid item xs={3} textAlign="center">
                    <Typography variant="body2" textAlign="center" fontWeight='bold'>
                        Total
                    </Typography>
                </Grid>
            </Grid>
            {
                !readMore ?
                    <Grid
                        item
                        container
                        padding="2px"
                        height='50px'
                        sx={{ borderBottom: 2, borderColor: '#E5E5E5' }}
                        alignItems="center"
                    >
                        <Grid item xs={1} textAlign="center">
                            <Typography variant="body2" textAlign="center">
                                {props.listOfDishes[0].dish.id}
                            </Typography>
                        </Grid>
                        <Grid item xs={4}>
                            <Typography variant="body2" textAlign="center">
                                {props.listOfDishes[0].dish.name}
                            </Typography>
                        </Grid>
                        <Grid item xs={2}>
                            <Typography variant="body2" textAlign="center">
                                {props.listOfDishes[0].dish.price}$
                            </Typography>
                        </Grid>
                        <Grid item xs={2}>
                            <Typography variant="body2" textAlign="center">
                                {props.listOfDishes[0].amount}
                            </Typography>
                        </Grid>
                        <Grid item xs={3} textAlign="center">
                            <Typography variant="body2">{(props.listOfDishes[0].dish.price * props.listOfDishes[0].amount).toFixed(2)}$</Typography>
                        </Grid>
                    </Grid>
                    :
                    props.listOfDishes.map((item: { dish: Dish, amount: number, status: 'Uncomplete' | 'Complete' }) => {
                        return (
                            <Grid
                                item
                                container
                                padding="2px"
                                height='50px'
                                sx={{ borderBottom: 2, borderColor: '#E5E5E5' }}
                                alignItems="center"
                                key={item.dish.id}
                            >
                                <Grid item xs={1} textAlign="center">
                                    <Typography variant="body2" textAlign="center">
                                        {item.dish.id}
                                    </Typography>
                                </Grid>
                                <Grid item xs={4}>
                                    <Typography variant="body2" textAlign="center">
                                        {item.dish.name}
                                    </Typography>
                                </Grid>
                                <Grid item xs={2}>
                                    <Typography variant="body2" textAlign="center">
                                        {item.dish.price}$
                                    </Typography>
                                </Grid>
                                <Grid item xs={2}>
                                    <Typography variant="body2" textAlign="center">
                                        {item.amount}
                                    </Typography>
                                </Grid>
                                <Grid item xs={3} textAlign="center">
                                    <Typography variant="body2">{(item.dish.price * item.amount).toFixed(2)}$</Typography>
                                </Grid>
                            </Grid>
                        )
                    })
            }
            <Grid item container xs={12} alignItems='center'>
                <Grid item xs={2} />
                <Grid item xs={8} textAlign='center'>
                    <ButtonComponent
                        color="titleColor"
                        size="small"
                        variant="text"
                        children={<Typography variant="body2">{!readMore ? "Read more" : "Hide"}</Typography>}
                        endIcon={readMore ? <KeyboardArrowUpIcon /> : <ExpandMoreIcon />}
                        onClick={handleChangeReadMore}
                    />
                </Grid>
                <Grid item xs={2} textAlign='right' padding='10px'>
                    <ButtonComponent
                        color="primaryColor"
                        size="medium"
                        variant="contained"
                        fullWidth
                        sx={{ borderRadius: '0px', borderBottomRightRadius: '10px' }}
                        children={<Typography variant="body2" fontWeight='bold'>Confirm</Typography>}
                        onClick={() => props.handleClerkConfirmPayment(props.id)}
                    />
                </Grid>
            </Grid>
        </Grid>
    )
}

export default ClerkPayment
