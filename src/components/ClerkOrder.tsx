import React, { useState } from 'react'
import {
  Checkbox,
  Grid,
  TextField,
  Typography,
} from '@mui/material'

import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';

import ButtonComponent from '../components/ButtonComponent'

import { DishCustomer } from '../reducers/temp/customer'

interface ClerkOrderProps {
  checked: boolean
  id: string
  listOfDishes: DishCustomer[]
  handleCheckOrder: (id: string) => Promise<void>
}

const ClerkOrder = (props: ClerkOrderProps) => {
  const [readMore, setReadMore] = useState<boolean>(false)
  const handleChangeReadMore = () => {
    setReadMore(!readMore)
  }

  return (
    <Grid container boxShadow={2} sx={{ borderRadius: '10px', paddingX: '20px', paddingY: '10px', backgroundColor: 'white' }}>
      <Grid
        item
        container
        justifyContent="space-between"
        alignItems="center"
        paddingY="5px"
      >
        <Typography variant="body1" fontWeight="bold">
          Order #{props.id}
        </Typography>
        {!props.checked && (
          <Checkbox
            sx={{
              color: 'black',
              padding: '0px',
              '&.Mui-checked': { color: '#A2F07B' },
            }}
            onClick={() => props.handleCheckOrder(props.id)}
          />
        )}
      </Grid>
      <Grid
        item
        container
        justifyContent="space-between"
        alignItems="center"
        paddingY="5px"
        paddingX="20px"
        sx={{ border: '1px solid #E5E5E5', borderRadius: '10px' }}
      >
        <Grid item container direction='row' alignItems='center' >
          {
            !readMore ? <>
              <Grid item xs={1}>
                <Checkbox
                  sx={{
                    color: 'black',
                    paddingLeft: '0px',
                    '&.Mui-checked': { color: '#A2F07B' },
                  }}
                  checked={props.listOfDishes[0].status === 'Complete'}
                  disabled={true}
                />
              </Grid>
              <Grid item xs={7}>
                <Typography variant="body2">
                  {props.listOfDishes[0].dish.name}
                </Typography>
              </Grid>
              <Grid item xs={2}>
                <Typography variant="body2">
                  Rp. {props.listOfDishes[0].dish.price}
                </Typography>
              </Grid>
              <Grid item xs={2} textAlign='right'>
                <TextField
                  disabled
                  sx={{ width: '30px' }}
                  inputProps={{
                    style: {
                      fontSize: '14px',
                      padding: '4px',
                      textAlign: 'center',
                    },
                  }}
                  defaultValue={props.listOfDishes[0].amount}
                />
              </Grid>
            </>
              :
              props.listOfDishes.map((item: DishCustomer) => (
                <>
                  <Grid item xs={1}>
                    <Checkbox
                      sx={{
                        color: 'black',
                        paddingLeft: '0px',
                        '&.Mui-checked': { color: '#A2F07B' },
                      }}
                      checked={item.status === 'Complete'}
                      disabled={true}
                    />
                  </Grid>
                  <Grid item xs={7}>
                    <Typography variant="body2">
                      {item.dish.name}
                    </Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography variant="body2">
                      Rp. {item.dish.price}
                    </Typography>
                  </Grid>
                  <Grid item xs={2} textAlign='right'>
                    <TextField
                      disabled
                      sx={{ width: '30px' }}
                      inputProps={{
                        style: {
                          fontSize: '14px',
                          padding: '4px',
                          textAlign: 'center',
                        },
                      }}
                      defaultValue={item.amount}
                    />
                  </Grid>
                </>
              ))}
        </Grid>
      </Grid>
      <Grid item container justifyContent="flex-end">
        <ButtonComponent
          color="titleColor"
          size="small"
          variant="text"
          children={<Typography variant="body2">{!readMore ? "Read more" : "Hide"}</Typography>}
          endIcon={readMore ? <KeyboardArrowUpIcon /> : <ExpandMoreIcon />}
          onClick={handleChangeReadMore}
        />
      </Grid>
    </Grid>
  )
}

export default ClerkOrder
