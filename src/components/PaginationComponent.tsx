import React from 'react'
import { Pagination, Grid } from '@mui/material'

interface PaginationComponentProps {
  count: number
  page?: number
  handleChangePage: (e: any, page: number) => void
}

const PaginationComponent = (props: PaginationComponentProps) => {
  return (
    <Grid container justifyContent='center'>
      <Pagination
        count={props.count}
        variant="outlined"
        shape="rounded"
        page={props.page}
        defaultPage={1}
        color="primaryColor"
        onChange={props.handleChangePage}
      />
    </Grid>
  )
}

export default PaginationComponent
