import React from 'react'
import { Grid, Typography } from '@mui/material'

interface PaymentItemProps {
  image: string | undefined
  name: string
  price: number
  quantity: number
}

const PaymentItem = (props: PaymentItemProps) => {
  return (
    <Grid
      container
      padding="2px"
      sx={{ border: 1, borderColor: '#E5E5E5', borderRadius: '20px', boxShadow: 1 }}
      alignItems="center"
    >
      <Grid item xs={2} textAlign="center">
        <img src={props.image} style={{ width: '50%' }} alt={props.name}/>
      </Grid>
      <Grid item xs={3}>
        <Typography variant="body2" textAlign="center">
          {props.name}
        </Typography>
      </Grid>
      <Grid item xs={2}>
        <Typography variant="body2" textAlign="center">
          {props.price}$
        </Typography>
      </Grid>
      <Grid item xs={2}>
        <Typography variant="body2" textAlign="center">
          {props.quantity}
        </Typography>
      </Grid>
      <Grid item xs={3} textAlign="center">
        <Typography variant="body2">{(props.price * props.quantity).toFixed(2)}$</Typography>
      </Grid>
    </Grid>
  )
}

export default PaymentItem
