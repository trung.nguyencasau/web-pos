import React from 'react'

import { useDispatch, useSelector } from 'react-redux'
import {
  Grid,
  Typography,
  Dialog,
  DialogContent,
  DialogTitle,
  TextField,
  List,
} from "@mui/material";
import RemoveOutlinedIcon from "@mui/icons-material/RemoveOutlined";
import ButtonComponent from "./ButtonComponent";
import CloseIcon from "@mui/icons-material/Close";

import ButtonIconComponent from "./ButtonIconComponent";
import AddOutlinedIcon from "@mui/icons-material/AddOutlined";

import { MINUS_AMOUNT_DISH, PLUS_AMOUNT_DISH, ADD_DISH_TO_ORDER } from '../constants'

interface ModalProductProps {
  id: string
  name: string
  price: number
  image: string
  isOpen: boolean
  description: string
  setOpen: React.Dispatch<React.SetStateAction<boolean>>
}

function ModalProduct(props: ModalProductProps) {
  const dispatch = useDispatch()
  const chosenDish = useSelector((state: any) => state.chosenDish)
  const handleClose = () => {
    props.setOpen(false)
  };

  const handlePlus = () => {
    dispatch({ type: PLUS_AMOUNT_DISH })
  }

  const handleMinus = () => {
    dispatch({ type: MINUS_AMOUNT_DISH })
  }

  const handleAddDishToOrder = () => {
    dispatch({ type: ADD_DISH_TO_ORDER, payload: chosenDish })
    props.setOpen(false)
  }

  return (
    <Dialog
      open={props.isOpen}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      sx={{ "&-paper": { borderRadius: "30px" }, maxHeight: "600px" }}
    >
      <DialogTitle
        sx={{ backgroundColor: "#ef1a69", paddingX: "20px", paddingY: "5px" }}
      >
        <Grid container justifyContent="space-between">
          <Grid item xs={4}>
            <Typography
              sx={{
                fontWeight: "500",
                fontSize: { xs: "20px", sm: "28px" },
                color: "white",
                lineHeight: "50px",
              }}
            >
              Add to cart
            </Typography>
          </Grid>
          <Grid item xs={1} container alignItems="center">
            <ButtonIconComponent
              children={<CloseIcon />}
              color="lightColor"
              size="small"
              backgroundColor="primaryColor"
              onClick={handleClose}
            />
          </Grid>
        </Grid>
      </DialogTitle>
      <DialogContent sx={{ paddingX: "20px" }}>
        <Grid container justifyContent="space-around">
          <Grid item xs={4}>
            <Grid container rowSpacing={5}>
              <Grid item xs={12} />

              <Grid item xs={12} justifyContent="center">
                <Grid container justifyContent="center">
                  <img src={props.image} width="100%" alt={props.name} />
                </Grid>
              </Grid>
              <Grid item xs={12}>
                <Grid container rowSpacing={3}>
                  <Grid item xs={12}>
                    <Grid container alignItems="center">
                      <Grid item xs={3}>
                        <Grid container justifyContent="flex-start">
                          <ButtonIconComponent
                            px={0.1}
                            py={0.1}
                            children={
                              <RemoveOutlinedIcon sx={{ fontSize: "14px" }} />
                            }
                            color="lightColor"
                            size="small"
                            backgroundColor={
                              chosenDish.amount === 1 ? 'textColor' : 'primaryColor'
                            }
                            onClick={handleMinus}
                          />
                        </Grid>
                      </Grid>
                      <Grid item xs={6}>
                        <TextField
                          value={chosenDish.amount}
                          disabled
                          inputProps={{
                            style: {
                              fontSize: "14px",
                              padding: "4px",
                              textAlign: "center",
                            },
                          }}
                        />
                      </Grid>
                      <Grid item xs={3}>
                        <Grid container justifyContent="flex-end">
                          <ButtonIconComponent
                            px={0.1}
                            py={0.1}
                            children={
                              <AddOutlinedIcon sx={{ fontSize: "14px" }} />
                            }
                            color="lightColor"
                            size="small"
                            backgroundColor="primaryColor"
                            onClick={handlePlus}
                          />
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid item xs={12} sx={{ padding: "0px" }}>
                    <ButtonComponent
                      fullWidth
                      variant="contained"
                      children={
                        <Typography variant="body1">Add to cart</Typography>
                      }
                      size="medium"
                      color="primaryColor"
                      onClick={handleAddDishToOrder}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={6}>
            <Typography
              sx={{
                fontSize: "24px",
                lineHeight: "50px",
                fontWeight: "500",
                paddingTop: "10px",
              }}
            >
              {props.name}
            </Typography>
            <Typography
              sx={{
                fontSize: "14px",
                lineHeight: "40px",
              }}
            >
              Rp: {props.price}
            </Typography>
            <List
              sx={{ maxHeight: "190px", width: "100%", overflow: "scroll" }}
            >
              <Typography
                variant="body2"
                fontWeight="light"
                align="justify"
                style={{ wordWrap: "break-word" }}
                padding="0px"
                paddingRight="5px"
              >
                {props.description}
              </Typography>
            </List>
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  );
}

export default ModalProduct;
