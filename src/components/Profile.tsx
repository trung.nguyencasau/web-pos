import React from 'react'
import { Grid, Typography } from '@mui/material'

import CreateIcon from '@mui/icons-material/Create'
import ExitToAppIcon from '@mui/icons-material/ExitToApp'
import ButtonComponent from './ButtonComponent'

interface PersonnalInformation {
  id: number
  name: string
  age: number
  gender: 'Male' | 'Female'
  email: string
  phone: string
  address: string
}
interface WorkInformation {
  restaurant: string
  role: 'clerk' | 'kitchen' | 'manager'
  join: string
}
interface ProfileProps {
  personal: PersonnalInformation
  work?: WorkInformation
}

const Profile = (props: ProfileProps) => {
  return (
    <Grid container maxWidth="lg" textAlign="center">
      <Grid item xs={12}>
        <Typography variant="h5" fontWeight="bold" padding="20px">
          Personal Information
        </Typography>
      </Grid>
      <Grid container columns={{ xs: 12 }} marginTop = '10px'>
        <Grid item xs={3} sm={4} textAlign="right">
          <Typography variant="body1" fontWeight="bold" padding="4px">
            ID:
          </Typography>
          <Typography variant="body1" fontWeight="bold" padding="4px">
            Name:
          </Typography>
          <Typography variant="body1" fontWeight="bold" padding="4px">
            Age:
          </Typography>
          <Typography variant="body1" fontWeight="bold" padding="4px">
            Gender:
          </Typography>
          <Typography variant="body1" fontWeight="bold" padding="4px">
            Email:
          </Typography>
          <Typography variant="body1" fontWeight="bold" padding="4px">
            Phone:
          </Typography>
          <Typography variant="body1" fontWeight="bold" padding="4px">
            Address:
          </Typography>
        </Grid>
        <Grid item xs={5} sm={4} textAlign="left">
          <Typography variant="body1" padding="4px">
            {props.personal.id}
          </Typography>
          <Typography variant="body1" padding="4px">
            {props.personal.name}
          </Typography>
          <Typography variant="body1" padding="4px">
            {props.personal.age}
          </Typography>
          <Typography variant="body1" padding="4px">
            {props.personal.gender}
          </Typography>
          <Typography variant="body1" padding="4px">
            {props.personal.email}
          </Typography>
          <Typography variant="body1" padding="4px">
            {props.personal.phone}
          </Typography>
          <Typography variant="body1" padding="4px">
            {props.personal.address}
          </Typography>
        </Grid>
        <Grid item xs={4} textAlign="right" paddingRight="20px">
          <img src="./upload.png" width="60%" style={{ borderRadius: '50%' }} alt="Profile avatar" />
          <ButtonComponent
            children="Change"
            size="medium"
            color="primaryColor"
            variant="contained"
            endIcon={<CreateIcon />}
          />
        </Grid>
      </Grid>
      {props.work && (
        <Grid container item xs={8}>
          <Grid item xs={12}>
            <Typography variant="h5" fontWeight="bold" padding="20px" marginTop = '10px'>
              Work Information
            </Typography>
          </Grid>
          <Grid container columns={{ xs: 8 }}>
            <Grid item xs={3} sm={4} textAlign="right">
              <Typography variant="body1" fontWeight="bold" padding="4px">
                Restaurant:
              </Typography>
              <Typography variant="body1" fontWeight="bold" padding="4px">
                Role:
              </Typography>
              <Typography variant="body1" fontWeight="bold" padding="4px">
                Join at:
              </Typography>
            </Grid>
            <Grid item xs={5} sm={4} textAlign="left">
              <Typography variant="body1" padding="4px">
                {props.work.restaurant}
              </Typography>
              <Typography variant="body1" padding="4px">
                {props.work.role}
              </Typography>
              <Typography variant="body1" padding="4px">
                {props.work?.join}
              </Typography>
            </Grid>
            {props.work.role === 'manager' && (
              <Grid item xs={12}>
                <ButtonComponent
                  children="Leave"
                  size="small"
                  color="primaryColor"
                  variant="contained"
                  endIcon={<ExitToAppIcon />}
                />
              </Grid>
            )}
          </Grid>
        </Grid>
      )}
    </Grid>
  )
}
export default Profile
