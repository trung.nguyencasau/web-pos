import React from "react";
import { Grid, Typography } from "@mui/material";

import FindInPageIcon from "@mui/icons-material/FindInPage";
import ButtonIconComponent from "./ButtonIconComponent";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";

interface DishForManagerProps {
  id: string;
  name: string;
  price: number;
  description: string;
  image: string
  handleViewDish: (id: string) => void
  handleEditDish: (id: string) => void
  deleteDish: (id: string) => Promise<void>
}

const DishForManager = (props: DishForManagerProps) => {
  return (
    <Grid
      container
      padding="2px"
      sx={{
        backgroundColor: "#F7F7F7",
        height: "70px",
        border: 1,
        borderColor: "#E5E5E5",
      }}
      alignItems="center"
    >
      <Grid item xs={1} textAlign="center">
        {props.id.substr(0, 10)}
      </Grid>

      <Grid item xs={2} textAlign="center">
        {props.name}
      </Grid>
      <Grid item xs={1} textAlign="center">
        {"$" + props.price}
      </Grid>

      <Grid
        item
        md={3}
        textAlign="center"
        sx={{ display: { xs: "none", lg: "block" }, m: 1 }}
      >
        <Typography noWrap>{props.description}</Typography>
      </Grid>

      <Grid item xs={2} textAlign="center">
        <img src={props.image} style={{ height: "60px" }} alt={props.name} />
      </Grid>

      <Grid item xs={5} lg={2} textAlign="center">
        <Grid container justifyContent="space-evenly">
          <ButtonIconComponent
            py={0.1}
            children={<FindInPageIcon sx={{ fontSize: "32px" }} />}
            color="lightColor"
            size="small"
            backgroundColor="greenColor"
            onClick={() => props.handleViewDish(props.id)}
          />
          <ButtonIconComponent
            py={0.1}
            children={<EditIcon sx={{ fontSize: "32px" }} />}
            color="lightColor"
            size="small"
            backgroundColor="editColor"
            onClick={() => props.handleEditDish(props.id)}
          />
          <ButtonIconComponent
            py={0.1}
            children={<DeleteIcon sx={{ fontSize: "32px" }} />}
            color="lightColor"
            size="small"
            backgroundColor="primaryColor"
            onClick={() => props.deleteDish(props.id)}
          />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default DishForManager;
