import React from "react";

import { IconButton, Box } from "@mui/material";

interface ButtonIconProps {
  backgroundColor:
    | "primaryColor"
    | "lightColor"
    | "textColor"
    | "editColor"
    | "greenColor"
    | "titleColor";
  children: React.ReactNode | undefined;
  size: "medium" | "small" | "large";
  color:
    | "primaryColor"
    | "lightColor"
    | "textColor"
    | "editColor"
    | "greenColor"
    | "titleColor";
  borderRadius?: number;
  px?: number;
  py?: number;
  disabled?: boolean;
  onClick?: (event: any | MouseEvent) => any;
}

const ButtonIconComponent = (props: ButtonIconProps) => {
  return (
    <Box
      sx={{
        display: "inline-block",
        backgroundColor: `${props.backgroundColor}.main`,
        px: `${props.px || 2}px`,
        py: `${props.py || 3}px`,
        borderRadius: `${props.borderRadius || 3}px`,
        "&: hover": {
          backgroundColor: `${props.backgroundColor}.main`,
          opacity: [0.9, 0.8, 0.7],
        },
      }}
      onClick={props.onClick}
    >
      <IconButton
        size={props.size}
        color={props.color}
        disabled={props.disabled}
      >
        {props.children}
      </IconButton>
    </Box>
  );
};

export default ButtonIconComponent;
