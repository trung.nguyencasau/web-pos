import React from 'react'

import { List, ListItem, Typography } from '@mui/material'

interface HeaderOptionProps {
  menuList: string[]
  type?: number
  handleChangeOptions: (index: number) => void
}

const HeaderOptionComponent = (props: HeaderOptionProps) => {
  return (
    <List sx={{ display: 'flex', p: 0 }}>
      {props.menuList.map((i, index) => {
        return (
          <ListItem
            disablePadding
            key={index}
            sx={{
              borderBottom: '1px solid',
              borderColor: `${
                props.type === index ? 'primaryColor.main' : 'lightColor.main'
              }`,
              mr: '5px',
              px: { md: 2, xs: 1 },
              pb: 0.5,
              '&:hover': {
                color: 'primaryColor.main',
              },
              transition: 'all 0.4s ease-in-out',
              cursor: 'pointer',
            }}
            onClick={() => props.handleChangeOptions(index)}
          >
            <Typography
              variant="body1"
              sx={{ fontSize: { xs: '12px', sm: '14px' } }}
              color={`${
                props.type === index ? 'primaryColor.main' : 'textColor.main'
              }`}
            >
              {i}
            </Typography>
          </ListItem>
        )
      })}
    </List>
  )
}

export default HeaderOptionComponent
