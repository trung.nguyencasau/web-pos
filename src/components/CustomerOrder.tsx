import React from 'react'
import { Grid, TextField, Typography, CardContent, Card } from '@mui/material'

import { useDispatch } from 'react-redux'

import ButtonIconComponent from './ButtonIconComponent'
import DeleteIcon from '@mui/icons-material/Delete'
import AddOutlinedIcon from '@mui/icons-material/AddOutlined'
import RemoveOutlinedIcon from '@mui/icons-material/RemoveOutlined'

import { PLUS_ORDER, MINUS_ORDER, REMOVE_DISH_FROM_ORDER } from '../constants'

interface CustomerOrderProps {
  id: string
  name: string
  price: number
  note?: string
  amount: number
}

const CustomerOrder = (props: CustomerOrderProps) => {
  const dispatch = useDispatch()
  const handlePlusOrder = () => {
    dispatch({ type: PLUS_ORDER, payload: props.id })
  }
  const handleMinusOrder = () => {
    dispatch({ type: MINUS_ORDER, payload: props.id })
  }
  const handleRemoveOrder = () => {
    dispatch({ type: REMOVE_DISH_FROM_ORDER, payload: props.id })
  }

  return (
    <Card
      sx={{ maxWidth: '320px', marginY: '5px', borderRadius: '10px' }}
      variant="outlined"
    >
      <CardContent sx={{ '&:last-child': { paddingBottom: '8px' } }}>
        <Grid container rowSpacing={2}>
          <Grid item xs={6}>
            <Typography sx={{ fontWeight: 'bold' }}>{props.name}</Typography>
            <Typography sx={{ color: '#d5d5d5' }}>Rp {props.price}</Typography>
          </Grid>
          <Grid item xs={6}>
            <Grid container alignItems="center">
              <Grid item xs={3}>
                <Grid container justifyContent="center">
                  <ButtonIconComponent
                    px={0.1}
                    py={0.1}
                    children={<RemoveOutlinedIcon sx={{ fontSize: '12px' }} />}
                    color="lightColor"
                    size="small"
                    backgroundColor={
                      props.amount === 1 ? 'textColor' : 'primaryColor'
                    }
                    onClick={handleMinusOrder}
                  />
                </Grid>
              </Grid>

              <Grid item xs={6}>
                <Grid container justifyContent="center">
                  <TextField
                    value={props.amount}
                    disabled
                    inputProps={{
                      style: {
                        fontSize: 12,
                        textAlign: 'center',
                        padding: '4px',
                      },
                    }}
                    fullWidth
                    sx={{ marginX: '4px' }}
                  />
                </Grid>
              </Grid>

              <Grid item xs={3}>
                <Grid container justifyContent="center">
                  <ButtonIconComponent
                    px={0.1}
                    py={0.1}
                    children={<AddOutlinedIcon sx={{ fontSize: '12px' }} />}
                    color="lightColor"
                    size="small"
                    backgroundColor="primaryColor"
                    onClick={handlePlusOrder}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={10}>
            <TextField
              placeholder="note"
              sx={{ backgroundColor: '#f7f7f7' }}
              inputProps={{
                style: { padding: '4px', paddingLeft: '10px', border: 'none' },
              }}
            />
          </Grid>
          <Grid item xs={2}>
            <ButtonIconComponent
              px={0.1}
              py={0.1}
              children={<DeleteIcon />}
              color="primaryColor"
              size="small"
              backgroundColor="lightColor"
              onClick={handleRemoveOrder}
            />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  )
}

export default CustomerOrder
