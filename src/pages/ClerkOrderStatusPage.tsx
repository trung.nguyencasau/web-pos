import React, { useEffect, useState } from 'react'
import { Grid, Typography } from '@mui/material'
import { useSelector } from 'react-redux';
import StaffHeader from '../components/StaffHeader';
import ClerkOrder from '../components/ClerkOrder';
import SideBarForEmployee from '../components/SideBarForEmployee';

import ClerkController from '../controllers/ClerkController';

import Order from '../models/Order'
import ClerkOrderComplete from '../components/ClerkOrderComplete';

const ClerkOrderStatusPage = () => {
    const auth = useSelector((state: any) => state.auth)

    const [orderAccepted, setOrderAccepted] = useState<Order[]>([])
    const [orderFinished, setOrderFinished] = useState<Order[]>([])
    const handleCheckOrder = async (id: string) => {
        console.log(id)
    }

    useEffect(() => {
        ClerkController.loadOrders(auth.restaurant.id, ['Accept'], new Date(), setOrderAccepted)
        ClerkController.loadOrders(auth.restaurant.id, ['Finish'], new Date(), setOrderFinished)
    }, [auth.restaurant.id])

    return (
        <Grid container>
            <Grid item xs={2}>
                <SideBarForEmployee />
            </Grid>
            <Grid item xs={10} padding='20px'>
                <Grid item marginBottom='10px'>
                    <StaffHeader page='Order' />
                </Grid>
                <Grid item container marginTop='10px' direction='row'>
                    <Grid item container direction='column' width={{ xs: '100%', md: '49%' }} minHeight='100vh' sx={{ backgroundColor: '#F7B2C5', borderRadius: '10px', paddingBottom: '10px', marginBottom: '20px' }}>
                        <Grid item sx={{ paddingX: '16px', paddingY: '8px' }}>
                            <Typography variant='h6' fontWeight='bold'>
                                Uncomplete
                            </Typography>
                        </Grid>
                        {
                            orderAccepted.map((item: Order) => {
                                return (
                                    <Grid item sx={{ paddingX: '16px', paddingY: '8px' }} key={item.id}>
                                        <ClerkOrder
                                            checked={true}
                                            id={item.id}
                                            listOfDishes={item.listOfDishes}
                                            handleCheckOrder={handleCheckOrder}
                                        />
                                    </Grid>

                                )
                            })
                        }
                    </Grid>
                    <Grid item width={{ md: '2%' }} />
                    <Grid item container direction='column' width={{ xs: '100%', md: '49%' }} minHeight='100vh' sx={{ backgroundColor: '#A2F07B', borderRadius: '10px', paddingBottom: '10px', marginBottom: '20px' }}>
                        <Grid item sx={{ paddingX: '16px', paddingY: '8px' }}>
                            <Typography variant='h6' fontWeight='bold'>
                                Complete
                            </Typography>
                        </Grid>
                        {
                            orderFinished.map((item: Order) => {
                                return (
                                    <Grid item
                                        sx={{ paddingX: '16px', paddingY: '8px' }}
                                        key={item.id}
                                    >
                                        <ClerkOrderComplete
                                            id={item.id}
                                        />
                                    </Grid>
                                )
                            })
                        }
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default ClerkOrderStatusPage