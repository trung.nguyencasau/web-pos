import React from 'react'
import { Grid } from '@mui/material'

import SideBarForEmployee from '../components/SideBarForEmployee';

import StaffHeader from '../components/StaffHeader';
import Profile from '../components/Profile';
import { useSelector } from 'react-redux';


const ProfilePage = () => {
    const { auth }: any = useSelector(state => state);

    return (
        <Grid container>
            <Grid item xs={2}>
                <SideBarForEmployee />
            </Grid>
            <Grid item xs={10} padding='20px'>
                <Grid item marginBottom='10px'>
                    <StaffHeader page='Profile' />
                </Grid>
                <Grid item>
                    <Profile personal={{
                        id: auth.id,
                        name: auth.name,
                        age: 20,
                        gender: 'Male',
                        email: auth.email,
                        phone: "0359322402",
                        address: "31 Nguyen Pham Tuan",
                    }}
                        work={{
                            restaurant: auth.restaurant ? auth.restaurant.name : "None",
                            role: auth.role,
                            join: "",
                        }}
                    />
                </Grid>
            </Grid>
        </Grid>
    )
}

export default ProfilePage