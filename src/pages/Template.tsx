import { Grid } from '@mui/material'
import React from 'react'
import SideBarComponent from '../components/SideBarComponent';

import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';
import AppRegistrationIcon from '@mui/icons-material/AppRegistration';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import PaymentIcon from '@mui/icons-material/Payment';
import DiningIcon from '@mui/icons-material/Dining';
import StaffHeader from '../components/StaffHeader';


const Template = () => {

    return (
        <Grid container>
            <Grid item xs = {2}>
                <SideBarComponent list={[{ icon: <AssignmentIndIcon />, label: 'Profile' },
                { icon: <AppRegistrationIcon />, label: 'Table' },
                { icon: <ShoppingCartIcon />, label: 'Order' },
                { icon: <PaymentIcon />, label: 'Payment' },
                { icon: <DiningIcon />, label: 'Kitchen' }]} />
            </Grid>
            <Grid item xs = {10} padding = '20px'>
                <Grid item marginBottom = '10px'>
                    <StaffHeader page = 'Page name'/>
                </Grid>
                <Grid item>
                    {/* //Code here */}
                </Grid>
            </Grid>
        </Grid>
    )
}

export default Template;