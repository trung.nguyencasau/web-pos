import React, { useEffect, useState } from "react";
import { Grid } from "@mui/material";
import { useSelector } from "react-redux";
import StaffHeader from "../components/StaffHeader";

import ClerkTable from "../components/ClerkTable";
import SideBarForEmployee from "../components/SideBarForEmployee";

import Table from '../models/Table'
import UserController from '../controllers/UserController'

const ClerkTablePage = () => {
  const auth = useSelector((state: any) => state.auth)
  const [clerkTable, setClerkTable] = useState<Table[]>([])

  const handleChangeTableStatus = async (id: string, status: 'Available' | 'Unavailable') => {
    status = status === 'Available' ? 'Unavailable' : 'Available'
    await UserController.changeTableStatus(auth.restaurant.id, id, status)
  }

  useEffect(() => {
    UserController.loadTables(auth.restaurant.id, setClerkTable)
  }, [auth.restaurant.id])

  return (
    <Grid container>
      <Grid item xs={2}>
        <SideBarForEmployee />
      </Grid>
      <Grid item xs={10} padding="20px">
        <Grid item marginBottom="10px">
          <StaffHeader page="Order" />
        </Grid>

        <Grid item container marginTop="10px" spacing={2}>
          {clerkTable.map((table: Table) => {
            return (
              <Grid
                key={table.id}
                item
                xs={6}
                md={3}
                sx={{ padding: "20px" }}
              >
                <ClerkTable
                  id={table.id}
                  status={table.status}
                  handleChangeTableStatus={handleChangeTableStatus} />
              </Grid>
            );
          })}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default ClerkTablePage;
