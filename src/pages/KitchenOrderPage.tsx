import React, { useEffect, useState } from 'react'
import { Grid } from '@mui/material'

import { useSelector } from 'react-redux';
import StaffHeader from '../components/StaffHeader';
import KitchenOrder from '../components/KitchenOrder';
import SideBarForEmployee from '../components/SideBarForEmployee';

import Order from '../models/Order'
import KitchenController from '../controllers/KitchenController'

const KitchenOrderPage = () => {
    const [orderList, setOrderList] = useState<Order[]>([])
    const auth = useSelector((state: any) => state.auth)

    const handleFinishDishInOrder = async (orderId: string, dishId: string) => {
        await KitchenController.finishDish(auth.restaurant.id, orderId, dishId)
    }

    useEffect(() => {
        KitchenController.loadAcceptedOrders(auth.restaurant.id, new Date(), setOrderList)
    }, [auth.restaurant.id])

    return (
        <Grid container>
            <Grid item xs={2}>
               <SideBarForEmployee />
            </Grid>
            <Grid item xs={10} padding='20px'>
                <Grid item marginBottom='10px'>
                    <StaffHeader page='Kitchen' />
                </Grid>
                <Grid item container direction='column' marginTop='10px' spacing={2}>
                    <Grid item>
                        {
                            orderList.map((item: Order) => {
                                return (
                                    <KitchenOrder
                                        key={item.id}
                                        id={item.id}
                                        listOfDishes={item.listOfDishes}
                                        handleFinishDishInOrder={handleFinishDishInOrder}
                                    />
                                )
                            })
                        }
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default KitchenOrderPage