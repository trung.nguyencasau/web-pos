import { createTheme } from '@mui/material/styles'

const theme = createTheme({
  palette: {
    primaryColor: {
      main: '#ef1a69',
      contrastText: '#ffffff',
    },
    lightColor: {
      main: '#ffffff',
    },
    titleColor: {
      main: '#000000',
      contrastText: '#ffffff',
    },
    textColor: {
      main: '#c4c4c4',
    },
    greenColor: {
      main: '#a2f07b',
      contrastText: '#ffffff',
    },
    editColor: {
      main: '#5971af',
      contrastText: '#ffffff',
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: 'none',
        },
        sizeSmall: {},
      },
    },
  },
})

declare module '@mui/material/styles' {
  interface Palette {
    primaryColor: Palette['primary']
    lightColor: Palette['primary']
    titleColor: Palette['primary']
    textColor: Palette['primary']
    greenColor: Palette['primary']
    editColor: Palette['primary']
  }

  // allow configuration using `createTheme`
  interface PaletteOptions {
    primaryColor?: PaletteOptions['primary']
    lightColor?: PaletteOptions['primary']
    titleColor?: PaletteOptions['primary']
    textColor?: PaletteOptions['primary']
    greenColor?: PaletteOptions['primary']
    editColor?: PaletteOptions['primary']
  }
}

declare module '@mui/material/Button' {
  interface ButtonPropsColorOverrides {
    primaryColor: true
    lightColor: true
    textColor: true
    editColor: true
    greenColor: true
    titleColor: true
  }
}

declare module '@mui/material/IconButton' {
  interface IconButtonPropsColorOverrides {
    primaryColor: true
    lightColor: true
    textColor: true
    editColor: true
    greenColor: true
    titleColor: true
  }
}

declare module '@mui/material/Box' {
  interface BoxPropsColorOverrides {
    primaryColor: true
    lightColor: true
    textColor: true
    editColor: true
    greenColor: true
    titleColor: true
  }
}
declare module '@mui/material/Pagination' {
  interface PaginationPropsColorOverrides {
    primaryColor: true
  }
}

export default theme
