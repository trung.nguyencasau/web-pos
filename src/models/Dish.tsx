type Mutable<T> = {
    -readonly [k in keyof T]: T[k];
};

export default class Dish {
    readonly id: string;
    readonly resId: string;
    readonly name: string;
    readonly price: number;
    readonly description: string;
    readonly status: 'Available' | 'Unavailable' | 'Delete';
    readonly image: string;
    readonly category: string;
    
    constructor(
        id: string,
        resId: string,
        name: string, 
        price: number, 
        description: string, 
        status: 'Available' | 'Unavailable' | 'Delete', 
        image: string,
        category: string = ''
    ) {
        this.id = id;
        this.resId = resId;
        this.name = name;
        this.price = price;
        this.description = description;
        this.status = status;
        this.image = image;
        this.category = category;
    }

    setStatus(status: 'Available' | 'Unavailable' | 'Delete') {
        const mutableThis = this as Mutable<Dish>;
        mutableThis.status = status;
    }

    static getDish(id: string): Dish{
        return new Dish('', '', '', 0, '', 'Available', '');
    }
}