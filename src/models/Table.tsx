type Mutable<T> = {
    -readonly [k in keyof T]: T[k];
};

export default class Table {
    readonly id: string;
    readonly status: 'Available' | 'Unavailable';

    constructor(
        id: string,
        status: 'Available' | 'Unavailable',
    ) {
        this.id = id;
        this.status = status;
    }

    setTable(
        id: string,
        status: 'Available' | 'Unavailable',
    ) {
        const mutableThis = this as Mutable<Table>;
        mutableThis.id = id;
        mutableThis.status = status;
    }
    
}