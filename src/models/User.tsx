import Restaurant from "./Restaurant";

export default class User {
    readonly id: string;
    readonly name: string;
    readonly image: string | null;
    readonly role: 'Manager' | 'Clerk' | 'Kitchen' | 'None';
    readonly email: string;
    readonly restaurant: Restaurant | null;
    
    constructor(
        id: string,
        name: string,
        image: string | null,
        role: 'Manager' | 'Clerk' | 'Kitchen' | 'None',
        email: string,
        restaurant: Restaurant | null,
    ) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.role = role;
        this.email = email;
        this.restaurant = restaurant;
    }
}