import Dish from "./Dish";
import Table from "./Table";

export default class Order {
    readonly id: string;
    readonly createAt: Date;
    readonly listOfDishes: { dish: Dish, amount: number, status: 'Uncomplete' | 'Complete' }[];
    readonly status: 'Not' | 'Accept' | 'Finish';
    readonly owner: string;
    readonly table: Table;
    readonly resId: string;

    constructor(
        id: string,
        createAt: Date,
        listOfDishes: { dish: Dish, amount: number, status: 'Uncomplete' | 'Complete' }[],
        status: 'Not' | 'Accept' | 'Finish',
        owner: string,
        table: Table,
        resId: string,
    ) {
        this.id = id;
        this.createAt = createAt;
        this.listOfDishes = listOfDishes;
        this.status = status;
        this.owner = owner;
        this.table = table;
        this.resId = resId;
    };

    editFood(dish: Dish, amount: number): boolean {
        for (var i = 0; i < this.listOfDishes.length; i++) {
            if (this.listOfDishes[i].dish.id === dish.id) {
                this.listOfDishes[i].amount += amount
                if (this.listOfDishes[i].amount <= 0) this.listOfDishes.splice(i, 1);
                return false;
            }
        }
        this.listOfDishes.push({
            dish: dish,
            amount: amount,
            status: 'Uncomplete',
        })
        return true;
    }


    static createOrder(resId: string, tableId: string, userId: string): Order {
        var id: string = Date.now().toString();
        return new Order(id, new Date(), [], "Not", userId, new Table(tableId, 'Available'), resId);
    }
}