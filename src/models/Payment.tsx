import Order from "./Order";

export default class Payment {
    readonly id: string;
    readonly createAt: Date;
    readonly order: Order;
    readonly status: 'Not' | 'Finish' | 'Accept';

    constructor(
        id: string,
        createAt: Date,
        order: Order,
        status: 'Not' | 'Finish' | 'Accept',
    ) {
        this.id = id;
        this.createAt = createAt;
        this.order = order;
        this.status = status;
    }
}