export default class Restaurant {
    readonly id: string;
    readonly name: string;
    readonly staffEmail: string[];
    readonly managerEmail: string;

    constructor(
        id: string,
        name: string,
        staffEmail: string[],
        managerEmail: string,
    ) {
        this.id = id;
        this.name = name;
        this.staffEmail = staffEmail;
        this.managerEmail = managerEmail;
    }
}